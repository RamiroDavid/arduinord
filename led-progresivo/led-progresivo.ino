int led=11;
void setup() {
  // put your setup code here, to run once:
pinMode(led, OUTPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
analogWrite(led, 0);
delay(100);
analogWrite(led, 64);
delay(100);
analogWrite(led, 127);
delay(100);
analogWrite(led, 191);
delay(100);
analogWrite(led, 255);
delay(100);

}
