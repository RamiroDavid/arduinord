

int pot=A0;
int led=11;
int val=0;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
  pinMode(pot, INPUT);

}

void loop() {
  // put your main code here, to run repeatedly:
  val=analogRead(pot)/4;
  analogWrite(led,val);
  delay(500);
  Serial.print(val);

}
