int potPin = 3;    // select the input pin for the potentiometer
int ledPin = 11;   // select the pin for the LED
int val = 0;       // variable to store the value coming from the sensor

void setup() {
  pinMode(ledPin, OUTPUT);  // declare the ledPin as an OUTPUT
  Serial.begin(9600);
}

void loop() {
  val = analogRead(potPin)/4;    // read the value from the sensor
  digitalWrite(ledPin, potPin);  // turn the ledPin on
  Serial.print(val);
  delay(2000);                  // stop the program for some time
  digitalWrite(ledPin, potPin);   // turn the ledPin off
  delay(2000);     // stop the program for some time
 
}
