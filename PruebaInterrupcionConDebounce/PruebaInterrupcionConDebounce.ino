const int tiempoUmbral = 200;
const int pinInterrupcion = 2;
volatile int contadorISR = 0;
int contador = 0;
long tiempoComienzo = 0;


void setup()
{
  pinMode(pinInterrupcion, INPUT_PULLUP);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(pinInterrupcion), contadorAntirrebote, FALLING);
}

void loop()
{
  if (contador != contadorISR)
  {
    contador = contadorISR;
    Serial.println(contador);
  }
  
}

void contadorAntirrebote()
{
  if (millis() - tiempoComienzo > tiempoUmbral)
  {
    contadorISR++;
    tiempoComienzo = millis();
  }
}
