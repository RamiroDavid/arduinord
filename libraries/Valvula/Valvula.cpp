#include "Arduino.h"
#include "Valvula.h"

Valvula::Valvula(int pin1Valvula, int pin2Valvula): _pin1Valvula(pin1Valvula), _pin2Valvula(pin2Valvula)
{	
}	

Valvula::init()
{
    pinMode(_pin1Valvula, OUTPUT);
    pinMode(_pin2Valvula, OUTPUT);
    digitalWrite(_pin2Valvula, LOW);
    Serial.print('Inicializacion exitosa valvula');
}

Valvula::activarValvula(int porcentajePWM, boolean progresivo, int valorEscalon)
{
    if (progresivo == false){
    digitalWrite(_pin1Valvula, HIGH);
   	analogWrite(11, porcentajePWM*255/100); //ENA pin
    } else {
            for(int i = 0; i < 4; i++)
            {
                digitalWrite(_pin1Valvula, HIGH);
   	            analogWrite(11, porcentajePWM*255/100); //ENA pin
                delay(20);
                porcentajePWM = porcentajePWM + valorEscalon;
            }
    }
}	

Valvula::desactivarValvula()
{
    digitalWrite(_pin1Valvula, LOW);
}	