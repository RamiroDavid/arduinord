	
#ifndef Valvula_h
#define Valvula_h

#include "Arduino.h"

class Valvula
{
  public:
    Valvula(int pin1Valvula, int pin2Valvula); //constructor
    init();
    activarValvula(int porcentajePWM, boolean progresivo, int valorEscalon);
    desactivarValvula();
    
  private:
   int _pin1Valvula;
   int _pin2Valvula;
};
#endif