#ifndef SensorPresion_h
#define SensorPresion_h
 
#include "Arduino.h"
#include <Wire.h>
 
class SensorPresion
{
  public:
      //int _valor;
    SensorPresion(uint8_t id);
    init();
    double LeerSensor();
    
  private:
    char unidades[10] = "cmH2O";
    uint8_t _id; // i2c address
    uint8_t data[7]; // holds output data
    const uint8_t cmd[3] = {0xAA, 0x00, 0x00}; // command to be sent
    double press_counts = 0; // digital pressure reading [counts]
    double pressure = 0; // pressure reading [bar, psi, kPa, etc.]
    const double outputmax = 15099494; // output at maximum pressure [counts]
    const double outputmin = 1677722; // output at minimum pressure [counts]
    double pmax = 70.307; // maximum value of pressure range [bar, psi, kPa, etc.]
    double pmin = -70.307; // minimum value of pressure range [bar, psi, kPa, etc.]
    double percentage = 0; // holds percentage of full scale data
    char printBuffer[200], cBuff[20], percBuff[20], pBuff[20], tBuff[20];



};
 
#endif