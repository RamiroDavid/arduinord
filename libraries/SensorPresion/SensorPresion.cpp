
#include "Arduino.h"
#include "SensorPresion.h"
 
SensorPresion::SensorPresion(uint8_t id): _id(id)
{
    
}

SensorPresion::init()
{
    Serial.begin(9600);
    while (!Serial) {
    delay(10);
    }
    Wire.begin();
    Serial.print('Inicializacion exitosa sensor presion');
}

double SensorPresion::LeerSensor()
{ 
    //uint8_t id = 0x28; //Si no pongo esta linea no funciona osea que el constructor no me fucniona
 //sprintf(printBuffer, "Pressure Output");
 //Serial.println(printBuffer);
 Wire.beginTransmission(_id);
 int stat = Wire.write (cmd, 3); // write command to the sensor
 stat |= Wire.endTransmission();
 delay(10);
 Wire.requestFrom(_id, (uint8_t) 7); // read back Sensor data 7 bytes. Casteo a uint8_t para que no me tire error en la compilacion
 int i = 0;
 for (i = 0; i < 7; i++) {
 data [i] = Wire.read();
 }
 press_counts = data[3] + data[2] * 256 + data[1] * 65536; // calculate digital pressure counts
 percentage = (press_counts / 16777215) * 100; // calculate pressure as percentage of full scale
 //calculation of pressure value according to equation 2 of datasheet
 pressure = ((press_counts - outputmin) * (pmax - pmin)) / (outputmax - outputmin) + pmin;
 dtostrf(press_counts, 4, 1, cBuff);
 dtostrf(percentage, 4, 3, percBuff);
 dtostrf(pressure, 4, 3, pBuff);

 /*
 The below code prints the raw data as well as the processed data
 Data format : Status Register, 24-bit Sensor Data, Digital Counts, percentage of full scale
pressure,
 pressure output, temperature
*/


 //sprintf(printBuffer, "% s % s \n", pBuff, unidades);
 //Serial.print(printBuffer);
 delay(10);
 return pressure;


}