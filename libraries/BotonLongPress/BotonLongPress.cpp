#include "Arduino.h"
#include "BotonLongPress.h"


BotonLongPress::BotonLongPress(int buttonPin, bool pullup = false): _buttonPin(buttonPin){
if (pullup == true) {
			pinMode(_buttonPin, INPUT_PULLUP);
		} else {
			pinMode(_buttonPin, INPUT);
		}
}
void BotonLongPress::clickEvent() {
  Serial.println("Click simple");
}
void BotonLongPress::doubleClickEvent() {
   Serial.println("Doble click");

}
void BotonLongPress::holdEvent() {
      Serial.println("Hold click");

}
void BotonLongPress::longHoldEvent() {
    Serial.println("Long hold click");

}
//=================================================
//  MULTI-CLICK:  One Button, Multiple Events


int BotonLongPress::checkButton() {    
   int event = 0;
   buttonVal = digitalRead(_buttonPin);
   // Button pressed down
   if (buttonVal == LOW && buttonLast == HIGH && (millis() - upTime) > debounce)
   {
       downTime = millis();
       ignoreUp = false;
       waitForUp = false;
       singleOK = true;
       holdEventPast = false;
       longHoldEventPast = false;
       if ((millis()-upTime) < DCgap && DConUp == false && DCwaiting == true)  DConUp = true;
       else  DConUp = false;
       DCwaiting = false;
   }
   // Button released
   else if (buttonVal == HIGH && buttonLast == LOW && (millis() - downTime) > debounce)
   {        
       if (not ignoreUp)
       {
           upTime = millis();
           if (DConUp == false) DCwaiting = true;
           else
           {
               event = 2;
               DConUp = false;
               DCwaiting = false;
               singleOK = false;
           }
       }
   }
   // Test for normal click event: DCgap expired
   if ( buttonVal == HIGH && (millis()-upTime) >= DCgap && DCwaiting == true && DConUp == false && singleOK == true && event != 2)
   {
       event = 1;
       DCwaiting = false;
   }
   // Test for hold
   if (buttonVal == LOW && (millis() - downTime) >= holdTime) {
       // Trigger "normal" hold
       if (not holdEventPast)
       {
           event = 3;
           waitForUp = true;
           ignoreUp = true;
           DConUp = false;
           DCwaiting = false;
           //downTime = millis();
           holdEventPast = true;
       }
   }
   buttonLast = buttonVal;
   return event;
}