	
#ifndef Motor_h
#define Motor_h

#include "Arduino.h"

class Motor
{
  public:
    Motor(int pin1Motor, int pin2Motor); //constructor
    init();
    activarMotor(int porcentajePWM);
    desactivarMotor();
    
  private:
   int _pin1Motor;
   int _pin2Motor;
   //int _pinENA;
};
#endif