#include "Arduino.h"
#include "Motor.h"

Motor::Motor(int pin1Motor, int pin2Motor):_pin1Motor(pin1Motor), _pin2Motor(pin2Motor)
{

}
Motor::init()
{
    pinMode(_pin1Motor, OUTPUT);
    pinMode(_pin2Motor, OUTPUT);
    digitalWrite(_pin2Motor, LOW);
    Serial.println("Inicializacion exitosa motor");
}
//Motor::activarMotor(float porcentajePWM)
Motor::activarMotor(int porcentajePWM)
{
    analogWrite(9, porcentajePWM*255/100); //ENA pin
    digitalWrite(_pin1Motor, HIGH); //Tiene que ser un pin que admita PWM
}	
Motor::desactivarMotor()
{
    digitalWrite(_pin1Motor, LOW);
}	