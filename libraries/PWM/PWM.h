	
#ifndef PWM_h
#define PWM_h

#include "Arduino.h"

class PWM
{
  public:
    PWM(int pinPWM, int valorPorcentaje); //constructor
    init();
    void modular();
	  int valorPorcentaje();
	  int valorPWM();
	  void asignarValorPorcentaje(int porcentaje);
    
  private:
    int _pinPWM;
    long _valorPorcentaje;

};
#endif
