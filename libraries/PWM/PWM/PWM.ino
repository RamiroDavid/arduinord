/*Ejemplo de modular salida analogica usando 
modulacion de ancho de pulso.
Se creo un archivo .h y uno .cpp creando la clase PWMr
Se usaron funciones nativas analogRead y analogWrite.

*/

#include <PWM.h>
 
PWM pwm(11,A0);
 
void setup()
{
  Serial.begin(9600);
}
 
void loop()
{
  pwm.modular();
  Serial.print(analogRead(A0)/4);
  Serial.print("-");
}
