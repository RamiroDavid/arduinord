#include "Arduino.h"
#include "PWM.h"

PWM::PWM(int pinPWM, int valorPorcentaje): _pinPWM(pinPWM), _valorPorcentaje(valorPorcentaje)
{
		
}	

PWM::init()
{
	pinMode(_pinPWM, OUTPUT);
	Serial.print('Inicializacion exitosa PWM');
}
void PWM::modular()
{ 
	//Asumiendo que es una salida digital de 10 bits=2^10=1024
	analogWrite(_pinPWM, _valorPorcentaje*255/100);
	//return _valorPorcentaje * 255 / 100; //Linea para control de valor resultante
}
int PWM::valorPorcentaje()
{
	return _valorPorcentaje;
}

int PWM::valorPWM()
{
	return _pinPWM;

}
void PWM::asignarValorPorcentaje(int porcentaje)
{
	_valorPorcentaje = porcentaje;
	return;

}