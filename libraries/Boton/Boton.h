// Editor .h de libreria Boton	
#ifndef Boton_h
#define Boton_h

#include "Arduino.h"

class Boton
{
  public:

    Boton(int pinBoton); 
    init();
    chequeoBoton();
    incrementarContador();
    uint8_t _contador;

  private:
 
    uint32_t _pinBoton; 
    bool _state;
	  bool _lastState;
	  uint16_t _debounceDelay;
	  uint32_t _lastDebounceTime;

};
#endif
