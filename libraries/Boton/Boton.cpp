//Editor .cpp libreria Boton
#include "Arduino.h"
#include "BotonLongPress.h"

Boton::Boton(int pinBoton): _pinBoton(pinBoton), _state(HIGH), _lastState(LOW), _debounceDelay(200), _lastDebounceTime(0), _contador(0){
	
}	

Boton::init()
{
	pinMode(_pinBoton, INPUT_PULLUP); //INPUT_PULLUP para interrupcion

}
Boton::incrementarContador()
{
	_contador++;
}
Boton::chequeoBoton()
{
	int reading = digitalRead(_pinBoton);
		// Checks if the buttons has changed state
		if (reading != _lastState) 
		{
			_lastDebounceTime = millis();
		}
		
		// Checks if the buttons hasn't changed state for '_debounceDelay' milliseconds.
		if ((millis() - _lastDebounceTime) > _debounceDelay) {
			// Checks if the buttons has changed state
			if (reading != _state) {
				_state = reading;
				
				if (_state == LOW) {
        		_contador++;
      			}
			} 
		}		
		_lastState = reading;
}
